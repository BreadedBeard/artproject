/*This will serve as a reminder of what to do

- Weather:Rain(no raining indoor)
- Improve Home(Make more house tiles)
- Swimming SFX(LOW PRIORITY)
- Death:spike(need SFX and clean up and death code, put this in a script too)
- Look at the map(Do this when it gets larger)
- Player emotes(Got the eyes)
- Save System
- Map/Teleportation?????? (LOW PRIORITY)
- Idle animations(Emotion dependent)
- Code to take away control from player(kinda got it)
-Player abilities:
    Double jump(DONE), high jump(just cut the jump speed), ground pound, magic missle(Particle effects and SFX),sticky feet(DONE)
- Fix indoor raining issues
-Debug mode:
    If things gets big, need a map list to teleport to(DONE)
    
-Optimising:    First step- Put certain event checks on timers(Look into obj_player and obj_playerimg) (DONE!)
                Make the water and vine calculation less taxing(I think it's okay now)
                Try to use with function
                Make lighting calculation less taxing
                Use alarms for blinking (DONE!)
                
Warning: DO NOT USE TOO MANY OBJ_ROPE_PARENT, USE MORE OBJ_GRASS INSTEAD
