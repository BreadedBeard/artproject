var map_width = 100;
var map_height = 100;
var map_rooms,map_status;
for(var i=0;i<map_width;i++){
    for(var j=0;j<map_height;j++){
        map_rooms[i,j] = -1;
        map_status[i,j] = 0;
    }
}
global.x_shift = floor(map_width/2);
global.y_shift = floor(map_height/2);

map_rooms[1+global.x_shift,1+global.y_shift] = h1v1;map_status[2+global.x_shift,1+global.y_shift] = 1;
map_rooms[2+global.x_shift,1+global.y_shift] = h2v1;map_status[1+global.x_shift,1+global.y_shift] = 1;
map_rooms[-2+global.x_shift,0+global.y_shift] = h_2v0;
map_rooms[-1+global.x_shift,0+global.y_shift] = h_1v0;
map_rooms[0+global.x_shift,0+global.y_shift] = h0v0;
map_rooms[1+global.x_shift,0+global.y_shift] = h1v0;
map_rooms[2+global.x_shift,0+global.y_shift] = h2v0;
map_rooms[3+global.x_shift,0+global.y_shift] = h3v0;map_status[3+global.x_shift,0+global.y_shift] = 1;
map_rooms[4+global.x_shift,0+global.y_shift] = h4v0;map_status[4+global.x_shift,0+global.y_shift] = 1;
map_rooms[-2+global.x_shift,-1+global.y_shift] = h_2v_1;
map_rooms[-1+global.x_shift,-1+global.y_shift] = h_1v_1;
map_rooms[0+global.x_shift,-1+global.y_shift] = h0v_1;
map_rooms[1+global.x_shift,-1+global.y_shift] = h1v_1;
map_rooms[2+global.x_shift,-1+global.y_shift] = h2v_1;

global.map_grid =  ds_grid_create(map_width, map_height);
global.map_indoor =  ds_grid_create(map_width, map_height);
for(var i=0;i<map_width;i++){
    for(var j=0;j<map_height;j++){
        ds_grid_set(global.map_grid,i,j,map_rooms[i,j]);
        ds_grid_set(global.map_indoor,i,j,map_status[i,j]);
    }
}

global.xmap = 0;
global.ymap = 0;
