var pos_x = argument0;
var pos_y = argument1;
var hspd = argument2;
var vspd = argument3;
var grav = argument4;
var grav_dir = argument5;
var imag_alp = argument6;
var imag_blnd = argument7;
var deletion_on_contact = argument8;
var part = argument9;
var drop;

    drop = instance_create(pos_x,pos_y,part);
    drop.vspeed = vspd;
    drop.hspeed = hspd;
    if grav>0 drop.gravity = grav;
    drop.gravity_direction = grav_dir;
    drop.image_alpha = imag_alp;
    drop.image_blend = imag_blnd;
    drop.destroy =deletion_on_contact; 
    
